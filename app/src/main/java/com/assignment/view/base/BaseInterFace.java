package com.assignment.view.base;


import androidx.databinding.ViewDataBinding;

public interface BaseInterFace {

     int getLayout();

     void initUI(ViewDataBinding binding);

}
