package com.assignment.view.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.assignment.R
import com.assignment.databinding.RowFlightsBinding
import com.assignment.model.bean.responses.Flight
import com.assignment.model.bean.responses.FlightsResponse
import com.assignment.utils.AppLogs
import java.lang.Exception
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FlightsAdapter(context: Context, private var allFlightResponse: FlightsResponse) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val layoutInflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return FlightsViewHolder(RowFlightsBinding.inflate(layoutInflater, parent, false))
    }

    override fun getItemCount(): Int {
        return allFlightResponse.flights?.size ?:0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as FlightsViewHolder).bind(allFlightResponse.flights?.get(position))
    }

    // All sorting done in ascending order
    fun sortFlights(type: SortType) = when(type){
        SortType.PRICE -> {
            //pick minimum price from all provider for flight and sorts all flight based on it
            try {
                (allFlightResponse.flights as? ArrayList)?.sortBy {
                    val minFareProvider = (it.fares as? ArrayList)?.minBy { it2 -> it2.fare ?: 0 }
                    AppLogs.i("Sort $type","${minFareProvider?.fare}")
                    minFareProvider?.fare
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            notifyDataSetChanged()
        }
        SortType.ARRIVAL_TIME->{
            try {
                (allFlightResponse.flights as? ArrayList)?.sortBy { Date(it.arrivalTime?:0) }
            }catch (e:Exception){
                e.printStackTrace()
            }
            notifyDataSetChanged()
        }
        SortType.DEPART_TIME->{
            try {
                (allFlightResponse.flights as? ArrayList)?.sortBy { Date(it.departureTime?:0) }
            }catch (e:Exception){
                e.printStackTrace()
            }
            notifyDataSetChanged()
        }
    }

    /** View Holders */
    inner class FlightsViewHolder(private val binding: RowFlightsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Flight?) {
            if(item==null) return

            binding.flightNameTv.text=getAirLineCode(item.airlineCode?:"")
            binding.travelClassTv.text=item.flightClass?:""
            binding.flightTimeTv.text="${milliToTime(item.departureTime?:0)} to ${milliToTime(item.arrivalTime?:0)}"
            binding.flightDurationTv.text=findDuration(item.departureTime?:0,item.arrivalTime?:0).replace("-","")
            binding.airportNameTv.text="${getAirportName(item.originCode)} to ${getAirportName(item.destinationCode)}"
            binding.dateTv.text=flightDate(item.departureTime?:0)
            binding.providersFaresLL.removeAllViews()
            if(item.fares.isNullOrEmpty()){
                binding.providersFaresLL.visibility=View.INVISIBLE
            }else {
                binding.providersFaresLL.visibility=View.VISIBLE
                item.fares?.forEach {
                    val fareView = LayoutInflater.from(itemView.context)
                        .inflate(R.layout.row_provider_fares, null)
                    fareView.findViewById<TextView>(R.id.providerNameTv).text =
                        getProviderName(it.providerId.toString())
                    fareView.findViewById<TextView>(R.id.priceTv).text = "Rs ${it.fare ?: "NA"}"
                    binding.providersFaresLL.addView(fareView)
                }
            }
        }
    }

    private fun findDuration(startTime: Long, endTime: Long): String {
        if (startTime == 0L || endTime == 0L) return ""
        try {
            val startDate = Date(startTime)
            val endDate = Date(endTime)
            val diff: Long = startDate.time - endDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            val duration=StringBuilder()
            duration.append("$hours hrs ")
            "${minutes.minus(hours*60)}".run {
                if(this.toIntOrNull()?:0>0)
                    duration.append("$this min")
            }
            return duration.toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    private fun milliToTime(timeInMillis: Long): String {
        try {
            val date = Date(timeInMillis)
            val sdf = SimpleDateFormat("HH:mm") //yyyy-MM-dd
            return sdf.format(date);
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
    private fun flightDate(timeInMillis: Long): String {
        try {
            val date = Date(timeInMillis)
            val sdf = SimpleDateFormat("dd MMM YYYY")
            return sdf.format(date);
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }
    private var airlineHashMap:HashMap<String,String>?=null
    private fun getAirLineCode(airlineCode: String): String? {
        if(airlineCode.isBlank()) return ""
        if (airlineHashMap == null)
            airlineHashMap = HashMap()
        if (airlineHashMap!!.contains(airlineCode))
            return airlineHashMap!![airlineCode]
        val name: String
        when (airlineCode) {
            "6E" -> {
                name = allFlightResponse.appendix?.airlines?.e6 ?: ""
            }
            "9W" -> {
                name = allFlightResponse.appendix?.airlines?.w9 ?: ""
            }
            "AI" -> {
                name = allFlightResponse.appendix?.airlines?.AI ?: ""
            }
            "G8" -> {
                name = allFlightResponse.appendix?.airlines?.G8 ?: ""
            }
            "SG" -> {
                name = allFlightResponse.appendix?.airlines?.SG ?: ""
            }
            else ->{name="NA"}
        }
        airlineHashMap!![airlineCode] = name
        return name

    }
    private var airportHashMap:HashMap<String,String>?=null

    private fun getAirportName(code: String?): String? {
        if(code.isNullOrBlank()) return ""
        if (airportHashMap == null)
            airportHashMap = HashMap()
        if (airportHashMap!!.contains(code))
            return airportHashMap!![code]
        val name: String = when (code) {
            "BOM" -> {
                allFlightResponse.appendix?.airports?.BOM ?: ""
            }
            "DEL" -> {
                allFlightResponse.appendix?.airports?.DEL ?: ""
            }
            else ->{
                "NA"
            }
        }
        airportHashMap!![code] = name
        return name

    }
    private var providerHashMap:HashMap<String,String>?=null

    private fun getProviderName(code: String): String? {
        if(code.isBlank()) return ""
        if (providerHashMap == null)
            providerHashMap = HashMap()
        if (providerHashMap!!.contains(code))
            return providerHashMap!![code]
        val name: String = when (code) {
            "1" -> {
                allFlightResponse.appendix?.providers?.provider1 ?: ""
            }
            "2" -> {
                allFlightResponse.appendix?.providers?.provider2 ?: ""
            }
            "3" -> {
                allFlightResponse.appendix?.providers?.provider3 ?: ""
            }
            "4" -> {
                allFlightResponse.appendix?.providers?.provider4 ?: ""
            }
            else ->{
                "NA"
            }
        }
        providerHashMap!![code] = name
        return name

    }
}
enum class SortType{
    PRICE,
    DEPART_TIME,
    ARRIVAL_TIME
}