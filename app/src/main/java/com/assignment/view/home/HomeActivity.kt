package com.assignment.view.home

import android.app.ProgressDialog
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.assignment.R
import com.assignment.databinding.ActivityHomeBinding
import com.assignment.model.bean.responses.FlightsResponse
import com.assignment.model.remote.ApiResponse
import com.assignment.utils.Connectivity
import com.assignment.utils.Utilities
import com.assignment.view.base.BaseActivity
import com.assignment.view.home.adapter.FlightsAdapter
import com.assignment.view.home.adapter.SortType
import com.assignment.view_model.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mBinding: ActivityHomeBinding
    private var adapter: FlightsAdapter?=null
    private val mViewModel: HomeViewModel by viewModel()
    private var allFlightsResponse: FlightsResponse?=null



    override fun getLayout(): Int {
        return R.layout.activity_home
    }

    override fun initUI(binding: ViewDataBinding?) {
        this.mBinding = binding as ActivityHomeBinding
        makeFlightApiCall()
        setUpClickListener()
    }

    private fun setUpClickListener() {
        mBinding.toolbar.backIv.setOnClickListener(this)
        mBinding.sortPriceTv.setOnClickListener(this)
        mBinding.sortArrivalTv.setOnClickListener(this)
        mBinding.sortDepartTv.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if(v==null) return

        when (v.id) {
            mBinding.sortArrivalTv.id -> {
                val type=SortType.ARRIVAL_TIME
                adapter?.sortFlights(type)
                mBinding.flightsRv.scrollToPosition(0)
            }
            mBinding.toolbar.backIv.id,mBinding.toolbar.backTv.id-> {
                finish()
            }
            mBinding.sortDepartTv.id -> {
                val type=SortType.DEPART_TIME
                adapter?.sortFlights(type)
                mBinding.flightsRv.scrollToPosition(0)
            }
            mBinding.sortPriceTv.id -> {
                val type=SortType.PRICE
                adapter?.sortFlights(type)
                mBinding.flightsRv.scrollToPosition(0)
            }
        }
    }

    private fun initRecyclerView() {
        mBinding.flightsRv.layoutManager = LinearLayoutManager(this)
        adapter = allFlightsResponse?.let { FlightsAdapter(this, it) }
        if (adapter != null)
            mBinding.flightsRv.adapter = adapter
    }

    private val flightsObserver: Observer<ApiResponse<FlightsResponse>> by lazy {
        Observer<ApiResponse<FlightsResponse>> {
            handleResponse(it)
        }
    }

    private fun makeFlightApiCall() {

        if (Connectivity.isConnected(this)) {
            /*** request viewModel to get data ***/
            mViewModel.callGetFlightsApi()
            /*** observe live data of viewModel*/
            mViewModel.getFlightsLiveData().observe(this, flightsObserver)
        } else {
            Utilities.showToast(this, resources.getString(R.string.no_network_error))

        }
    }

    /* Response Handlers */
    private fun handleResponse(response: ApiResponse<FlightsResponse>) {
        when (response.status) {
            ApiResponse.Status.LOADING -> {
                progressDialog.show()
            }
            ApiResponse.Status.SUCCESS -> {
                progressDialog.dismiss()
                if (response.data!=null) {
                    allFlightsResponse=response.data
                    initRecyclerView()
                } else {
                    Utilities.showToast(this, getString(R.string.no_data_found))
                }
            }

            ApiResponse.Status.ERROR -> {
                progressDialog.dismiss()
                Utilities.showToast(this, response.errorMessage.toString())
            }
        }
    }

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage(getString(R.string.please_wait_msg))
        }
    }

}