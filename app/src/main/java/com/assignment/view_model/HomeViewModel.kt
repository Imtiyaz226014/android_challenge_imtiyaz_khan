package com.assignment.view_model

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.assignment.MyApplication
import com.assignment.model.bean.responses.FlightsResponse
import com.assignment.model.remote.ApiResponse
import com.assignment.model.repo.HomeRepository

class HomeViewModel constructor(app: MyApplication, private var homeRepository: HomeRepository) : AndroidViewModel(app){
    private var flightsLiveData = MutableLiveData<ApiResponse<FlightsResponse>>()

    fun callGetFlightsApi() {
        homeRepository.getAllFlights(flightsLiveData)
    }
    fun getFlightsLiveData(): MutableLiveData<ApiResponse<FlightsResponse>> {
        return flightsLiveData
    }
}