package com.assignment.model.bean.responses

import com.google.gson.annotations.SerializedName

data class FlightsResponse(
    val appendix: Appendix?,
    val flights: List<Flight>?
)

class Appendix(
    @SerializedName("airlines") val airlines: Airlines,
    @SerializedName("airports") val airports: Airports,
    @SerializedName("providers") val providers: Providers
)

class Flight(
    @SerializedName("airlineCode") val airlineCode: String?,
    @SerializedName("arrivalTime")  val arrivalTime: Long?,
    @SerializedName("class")  val flightClass: String?,
    @SerializedName("departureTime")  val departureTime: Long?,
    @SerializedName("destinationCode") val destinationCode: String?,
    @SerializedName("fares") val fares: List<Fare>?,
    @SerializedName("originCode") val originCode: String?
)

class Airlines(
    @SerializedName("6E") val e6: String,
    @SerializedName("9W") val w9: String,
    @SerializedName("AI") val AI: String,
    @SerializedName("G8") val G8: String,
    @SerializedName("SG") val SG: String
)

class Airports(
    @SerializedName("BOM") val BOM: String,
    @SerializedName("DEL") val DEL: String
)

class Providers(
    @SerializedName("1") val provider1: String,
    @SerializedName("2") val provider2: String,
    @SerializedName("3") val provider3: String,
    @SerializedName("4") val provider4: String
)
//`4`

class Fare(
    @SerializedName("fare") val fare: Int?,
    @SerializedName("providerId") val providerId: Int
)