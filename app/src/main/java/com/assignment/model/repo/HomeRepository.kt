package com.assignment.model.repo

import androidx.lifecycle.MutableLiveData
import com.assignment.model.bean.responses.FlightsResponse
import com.assignment.model.remote.ApiResponse
import com.assignment.model.remote.ApiServices
import com.assignment.model.remote.DataFetchCall
import kotlinx.coroutines.Deferred
import org.koin.core.KoinComponent
import retrofit2.Response


class HomeRepository constructor(
    private val apiServices: ApiServices
) : KoinComponent {

    fun getAllFlights(
        responseObservable: MutableLiveData<ApiResponse<FlightsResponse>>
    ) {
        object : DataFetchCall<FlightsResponse>(responseObservable) {

            override fun createCallAsync(): Deferred<Response<FlightsResponse>> {
                return apiServices.getAllFlights()
            }

            override fun saveResult(result: FlightsResponse) {
            }
        }.execute()
    }


}