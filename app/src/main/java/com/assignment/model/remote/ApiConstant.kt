package com.assignment.model.remote

class ApiConstant {

    companion object {
        /*********API BASE URL************/
        const val BASE_URL = "http://www.mocky.io/"
        const val API_TIME_OUT: Long = 6000
        const val GET_FLIGHTS = "v2/5979c6731100001e039edcb3"
    }

}