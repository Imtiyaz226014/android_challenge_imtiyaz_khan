package com.assignment.model.remote

import com.assignment.model.bean.responses.FlightsResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ApiServices {

    @GET(ApiConstant.GET_FLIGHTS)
    fun getAllFlights(): Deferred<Response<FlightsResponse>>
}